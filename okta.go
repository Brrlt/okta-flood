package main

import (
  "io/ioutil"
   "fmt"
   "os"
   "bytes"
   "net/http"
   "sync"
   "log"
   "strconv"
   "flag"
   "time"
   "encoding/json"
   "runtime"
)
//https://trial-4003044.okta.com/api/v1/users
//00EsNGEHgEbe7OW11KdZ7AT3omjqzaVcTjd_jzSn34

type OktaUser struct {
	ID              string    `json:"id"`
	Status          string    `json:"status"`
	Created         time.Time `json:"created"`
	Activated       time.Time `json:"activated"`
	StatusChanged   time.Time `json:"statusChanged"`
	LastLogin       any       `json:"lastLogin"`
	LastUpdated     time.Time `json:"lastUpdated"`
	PasswordChanged time.Time `json:"passwordChanged"`
	Type            struct {
		ID string `json:"id"`
	} `json:"type"`
	Profile struct {
		FirstName   string `json:"firstName"`
		LastName    string `json:"lastName"`
		MobilePhone any    `json:"mobilePhone"`
		SecondEmail any    `json:"secondEmail"`
		Login       string `json:"login"`
		Email       string `json:"email"`
	} `json:"profile"`
	Credentials struct {
		Password struct {
		} `json:"password"`
		Provider struct {
			Type string `json:"type"`
			Name string `json:"name"`
		} `json:"provider"`
	} `json:"credentials"`
	Links struct {
		Suspend struct {
			Href   string `json:"href"`
			Method string `json:"method"`
		} `json:"suspend"`
		Schema struct {
			Href string `json:"href"`
		} `json:"schema"`
		ResetPassword struct {
			Href   string `json:"href"`
			Method string `json:"method"`
		} `json:"resetPassword"`
		ForgotPassword struct {
			Href   string `json:"href"`
			Method string `json:"method"`
		} `json:"forgotPassword"`
		ExpirePassword struct {
			Href   string `json:"href"`
			Method string `json:"method"`
		} `json:"expirePassword"`
		ChangeRecoveryQuestion struct {
			Href   string `json:"href"`
			Method string `json:"method"`
		} `json:"changeRecoveryQuestion"`
		Self struct {
			Href string `json:"href"`
		} `json:"self"`
		ResetFactors struct {
			Href   string `json:"href"`
			Method string `json:"method"`
		} `json:"resetFactors"`
		Type struct {
			Href string `json:"href"`
		} `json:"type"`
		ChangePassword struct {
			Href   string `json:"href"`
			Method string `json:"method"`
		} `json:"changePassword"`
		Deactivate struct {
			Href   string `json:"href"`
			Method string `json:"method"`
		} `json:"deactivate"`
	} `json:"_links"`
}

type OktaGroup []struct {
	ID                    string    `json:"id"`
	Created               time.Time `json:"created"`
	LastUpdated           time.Time `json:"lastUpdated"`
	LastMembershipUpdated time.Time `json:"lastMembershipUpdated"`
	ObjectClass           []string  `json:"objectClass"`
	Type                  string    `json:"type"`
	Profile               struct {
		Name        string `json:"name"`
		Description any    `json:"description"`
	} `json:"profile"`
	Links struct {
		Logo []struct {
			Name string `json:"name"`
			Href string `json:"href"`
			Type string `json:"type"`
		} `json:"logo"`
		Users struct {
			Href string `json:"href"`
		} `json:"users"`
		Apps struct {
			Href string `json:"href"`
		} `json:"apps"`
	} `json:"_links"`
}

func create_okta_user(i int, name string, oktaurl string, oktaapikey string) (*http.Request){
	var data = []byte(`{"profile": {
		"firstName": "`+name+strconv.Itoa(i)+`",
		"lastName": "`+name+strconv.Itoa(i)+`",
		"email": "`+name+strconv.Itoa(i)+`@bali.local",
		"login": "`+name+strconv.Itoa(i)+`@bali.local"
	  },
	  "credentials": {
		"password" : { "value": "P@ssw0rd" }
	  }
	}`)
	req, _ := http.NewRequest("POST", oktaurl+"/users", bytes.NewBuffer(data))
	query := req.URL.Query()
	query.Add("activate", "true")
	query.Add("provider", "false")
	query.Add("nextLogin", "changePassword")
	req.URL.RawQuery = query.Encode()

	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Authorization", "SSWS " + oktaapikey)
	return req
}

func add_user_to_group(group string, user string, oktaurl string, oktaapikey string) (*http.Request){
	req, _ := http.NewRequest("PUT", oktaurl+"/groups/"+group+"/users/"+user, nil)
	query := req.URL.Query()
	req.URL.RawQuery = query.Encode()

	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Authorization", "SSWS " + oktaapikey)
	return req
}

func get_group_id(group string, oktaurl string, oktaapikey string) (string){
	req, _ := http.NewRequest("GET", oktaurl+"/groups?q="+group+"&limit=1", nil)
	query := req.URL.Query()
	req.URL.RawQuery = query.Encode()
	req.Header.Add("Accept", "application/json")
	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Authorization", "SSWS " + oktaapikey)
	res, _ := http.DefaultClient.Do(req)
	defer res.Body.Close()
	body, _ := ioutil.ReadAll(res.Body)
	var result OktaGroup
	if err := json.Unmarshal(body, &result); err != nil {
		log.Println("Can not unmarshal JSON")
	}
	return result[0].ID
}

func delete_user(i int, user string, oktaurl string, oktaapikey string)(*http.Request){
	req, _ := http.NewRequest("DELETE", oktaurl+"/users/"+user+strconv.Itoa(i)+"@bali.local", nil)
	query := req.URL.Query()
	req.URL.RawQuery = query.Encode()

	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Authorization", "SSWS " + oktaapikey)
	return req
}

func handle_request(req *http.Request) ([]byte){
	res, err := http.DefaultClient.Do(req)
	if err != nil {
		log.Println("No response from okta, please verify Okta URL and ApiKey")
		os.Exit(1)
	}else {
		body, _ := ioutil.ReadAll(res.Body)
		bodyString := string(body)
		defer res.Body.Close()
		if res.StatusCode != http.StatusOK && res.StatusCode != http.StatusNoContent {
			log.Fatal("API request error", bodyString)
		}
		return body
	}
	return nil
}

func request(start int, step int, max int, username string, deleteuser bool, groupID string, oktaurl string, oktaapikey string){
	log.Println("Thread " + strconv.Itoa(start) + " started")
	for j := start; j <= max; j+=step {
		if deleteuser{
			req := delete_user(j, username, oktaurl, oktaapikey)
			log.Println("Deleted user : " + username+strconv.Itoa(j))
			handle_request(req)
		}else{
			req := create_okta_user(j, username, oktaurl, oktaapikey)
			body := handle_request(req)
			log.Println("Created user : " + username+strconv.Itoa(j))
			var oktauser OktaUser
			if err := json.Unmarshal(body, &oktauser); err != nil {
				log.Println("Can not unmarshal JSON")
			}
			log.Println(oktauser)
			req = add_user_to_group(groupID, oktauser.ID, oktaurl, oktaapikey)
			body = handle_request(req)
			log.Println("Added user " + username+strconv.Itoa(j) + " to group")
		}
	}
}

func setArgs() (string, string, bool, string, int, string, int) {
	var ou string
	var oak string
	var du bool
	var u string
	var an int
	var gn string
	var tn int
	flag.StringVar(&ou, "url", "null", "Okta URL")
	flag.StringVar(&ou, "u", "null", "Okta URL")
	flag.StringVar(&oak, "apikey", "null", "Okta ApiKey")
	flag.StringVar(&oak, "a", "null", "Okta ApiKey")
	flag.StringVar(&gn, "group", "null", "Specify group name")
	flag.StringVar(&gn, "g", "null", "Specify group name")
	flag.BoolVar(&du, "delete", false, "Use script to delete users")
	flag.BoolVar(&du, "d", false, "Use script to delete users")
	flag.StringVar(&u, "name", "user", "Specify user name")
	flag.StringVar(&u, "n", "user", "Specify user name")
	flag.IntVar(&an, "count", 100, "Specify number of account to create")
	flag.IntVar(&an, "c", 100, "Specify number of account to create")
	flag.IntVar(&tn, "thread", 4, "Specify number of thread per cpu to create, use t=0 for one global thread (default 4 threads per cpu)")
	flag.IntVar(&tn, "t", 4, "Specify number of thread per cpu to create, use t=0 for one global thread (default 4 threads per cpu)")
	const usage = `Usage of floodokta:
    -a, --apikey  Okta ApiKey (mandatory)
    -u, --url     Okta URL (mandatory)
	-g, --group   Specify group name (mandatory)
	-d , --delete User script to delete users (default false)
	-n, --name    Specify user name (default user)
	-c, --count   Specify number of account to create (default 100)
	-t, --thread   Specify number of thread per cpu to create, use t=0 for one global thread (default 4 threads per cpu)
`
	flag.Usage = func() { fmt.Print(usage) }
	flag.Parse()
	return ou, oak, du, u, an, gn, tn
}
func main(){
	ou, oak, du, u, an, gn, tn:= setArgs()
	cpu := runtime.NumCPU()
	if ou == "null" || oak == "null" || gn == "null"{
		fmt.Println("Please provide valid Okta URL, ApiKey and group name")
		os.Exit(1)
	}
	log.Println("Using Okta URL " + ou)
	log.Println("Using Okta ApiKey " + oak)
	gi := get_group_id(gn, ou, oak)
	steps := 1 
	if tn != 0{
		steps = cpu*tn
	}
	log.Println("Starting " + strconv.Itoa(steps) + " threads on " + strconv.Itoa(cpu) + " different cpu")
	var wg sync.WaitGroup
	for i := 1; i <= steps; i++ {
		wg.Add(1)
		i := i
		go func() {
            defer wg.Done()
			request(i, steps, an, u, du, gi, ou, oak)
		}()
	}
	wg.Wait() 
}
