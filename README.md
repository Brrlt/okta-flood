# TP Okta API


## Usage

Les options suivantes sont disponibles pour l'utilisation du programme.
```
    -a, --apikey  Okta ApiKey (mandatory)
    -u, --url     Okta URL (mandatory)
    -g, --group   Specify group name (mandatory)
    -d , --delete User script to delete users (default false)
    -n, --name    Specify user name (default user)
    -c, --count   Specify number of account to create (default 100)
    -t, --thread   Specify number of thread per cpu to create (default 4)
```

Exemple d'utilisation :
```
go run okta.go -a APIKEY -u URL -g oktasynchro
```

Les options `-a`, `-u` et `-g` sont obligatoires, les options `-c`, `-n` et `-t` ont des valeurs par défaut et l'options `-d` est optionnelle.

L'option `-u` doit contenir l'URL de la racine API d'okta (exemple : `https://trial-4003044.okta.com/api/v1`).  
L'option `-g` doit contenir le nom du groupe qui doit être ajouté aux comptes utilisateurs (Le nom du groupe de synchronisation avec l'AD).  
L'option `-c` permet de spécifier le nombre d'utilisateurs à créer.  
L'option `-d` permet de désactiver / supprimer les comptes utilisateurs.  
L'option `-t` permet de spécifier un nombre de threads spécifiques.  

Les comptes sont créés avec le nom d'utilisateur `userXX` par défaut. L'option `-n` permet de spécifier un nom d'utilisateur.  
Le programme exécute les requêtes avec par défaut `nombre_de_cpu*4` threads (Les requêtes à l'API okta sont bloquantes pendant une durée assez importante, on optimise ainsi le temps d'exécution). Comme une seule clé d'API est utilisée, la limite de requêtes concurrentes par clé d'API est rapidement atteinte et tous les comptes ne sont pas bien créés.

Le programme s'arrête en cas d'erreur renvoyée par l'API.
